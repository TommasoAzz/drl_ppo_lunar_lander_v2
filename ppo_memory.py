import os
from typing import List, Tuple
import numpy as np
import torch as T
import torch.nn as nn
import torch.optim as optim
from torch.distributions.categorical import Categorical


class PPOMemory:
    def __init__(self, batch_size: int) -> None:
        self.states = []
        self.probs = []
        self.vals = []
        self.actions = []
        self.rewards = []
        self.dones = []

        self.batch_size = batch_size

    def generate_batches(self) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray, List[np.ndarray]]:
        n_states = len(self.states)
        # Generates the indexes of where each batch starts
        batch_start = np.arange(0, n_states, self.batch_size)
        # = range(n_states)
        indices = np.arange(n_states, dtype=np.int64)
        np.random.shuffle(indices)
        # Generates batches with indices
        batches = [indices[i:i+self.batch_size] for i in batch_start]

        return (
            np.array(self.states),
            np.array(self.actions),
            np.array(self.probs),
            np.array(self.vals),
            np.array(self.rewards),
            np.array(self.dones),
            batches
        )

    def store_transition(self, state, action: int, prob: float, val, reward: float, done: bool):
        self.states.append(state)
        self.actions.append(action)
        self.probs.append(prob)
        self.vals.append(val)
        self.rewards.append(reward)
        self.dones.append(done)

    def clear_memory(self):
        self.states.clear()
        self.probs.clear()
        self.vals.clear()
        self.actions.clear()
        self.rewards.clear()
        self.dones.clear()