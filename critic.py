import os
from turtle import forward
from typing import List, OrderedDict, Tuple
import numpy as np
import torch as T
import torch.nn as nn
import torch.optim as optim
from torch.distributions.categorical import Categorical

class Critic(nn.Module):
    def __init__(self,
                 input_dims: int,
                 lr: int,
                 checkpoint_file_path: str,
                 fc1_dims: int = 256,
                 fc2_dims: int = 256) -> None:
        super().__init__()

        self.checkpoint_file_path = checkpoint_file_path

        self.network = nn.Sequential(OrderedDict({
            "linear_1": nn.Linear(input_dims, fc1_dims),
            "relu_1": nn.ReLU(),
            "linear_2": nn.Linear(fc1_dims, fc2_dims),
            "relu_2": nn.ReLU(),
            "linear_3": nn.Linear(fc2_dims, 1)
        }))

        self.optimizer = optim.Adam(self.parameters(), lr=lr)
        self.device = T.device("cuda:0" if T.cuda.is_available() else "cpu")
        self.to(self.device)

    def forward(self, x: T.Tensor) -> T.Tensor:
        return self.network.forward(x)

    def save_checkpoint(self) -> None:
        T.save(self.state_dict(), self.checkpoint_file_path)

    def load_checkpoint(self) -> None:
        self.load_state_dict(T.load(self.checkpoint_file_path))