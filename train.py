import gym
import numpy as np

from agent import Agent


if __name__ == '__main__':
    env = gym.make("LunarLander-v2")

    N = 20
    batch_size = 5
    n_epochs = 4
    lr = 0.0003
    n_games = 500

    agent = Agent(n_actions=env.action_space.n, batch_size=batch_size, lr=lr,
                  n_epochs=n_epochs, input_dims=env.observation_space.shape[0],
                  model_storage_folder=".")

    best_score = env.reward_range[0]
    score_history = []

    learn_iterations = 0
    average_score = 0
    n_steps = 0

    for i in range(n_games):
        state = env.reset()
        done = False
        score = 0
        while not done:
            action, prob, val = agent.choose_action(state)
            new_state, reward, done, info = env.step(action)
            n_steps += 1
            score += reward
            agent.store_transition(state, action, prob, val, reward, done)
            if n_steps % N == 0:
                agent.learn()
                learn_iterations += 1
            state = new_state
        score_history.append(score)
        average_score = np.mean(score_history[-100:])

        if average_score > best_score:
            print("Updating best score and saving checkpoint!")
            best_score = average_score
            agent.save_checkpoint()

        print("Episode", i, "\tscore %.1f" % score, "\tavg score %.1f" % average_score, "\ttimesteps", n_steps, "\tlearning steps", learn_iterations)

    for i in range(20):
        state = env.reset()
        env.render()
        done = False
        score = 0
        while not done:
            action, prob, val = agent.choose_action(state)
            new_state, reward, done, info = env.step(action)
            env.render()
            score += reward
            state = new_state